import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import GetEstimate from '@/views/GetEstimate'
import Estimate from '@/views/Estimate'
import Pickup from '@/views/Pickup'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/estimate',
    // name: 'About',
    component: GetEstimate,
    
    children: [
      {path: '', component: Estimate},
      {path: 'pickup', component: Pickup},
    ]
  },
  {
    path: '/pickup',
    name: 'Pickup',
    component: () => import(/* webpackChunkName: "about" */ '../views/Pickup.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
